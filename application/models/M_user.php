<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	public function login($post)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('username', $post['username']);
        $this->db->where('password', $post['password']);
        $query = $this->db->get();
        return $query;
    }

    public function get($id = null)
    {
        $this->db->from('user');
        if($id != null){
            $this->db->where('user_id', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function add($post)
    {
        $data['name'] = $post['name'];
        $data['username'] = $post['username'];
        $data['password'] = $post['password'];
        $data['address'] = $post['address'] != "" ? $post['address'] : null;
        $data['hak_akses'] = $post['hak_akses'];

        $this->db->insert('user', $data);
    }

    public function hapus($id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete('user');
    }
}
