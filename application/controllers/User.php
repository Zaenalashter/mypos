<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		check_not_login();
        $this->load->model('M_user');
	}

	public function index()
	{
        $data ['row'] = $this->M_user->get();

		$this->template->load('template', 'user/user_data', $data);
	}

    public function add()
    {
		// print_r($_POST['username']);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|is_unique[user.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
		$this->form_validation->set_rules('passconf', 'Konfirmasi Password', 'required|matches[password]');
		$this->form_validation->set_rules('hak_akses', 'Hak Akses', 'required');

		$this->form_validation->set_message('required', '%s harus di isi !');
		$this->form_validation->set_message('min_length', '%s minimal 5 karakter !');
		$this->form_validation->set_message('matches', '%s tidak sesuai dengan password !');
		$this->form_validation->set_message('is_unique', '%s sudah dipakai !');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
		
		if($this->form_validation->run() == FALSE){
			$this->template->load('template', 'user/user_add');
		}else{
			$post = $this->input->post(null, TRUE);
			$this->M_user->add($post);
			if($this->db->affected_rows() > 0){
				echo " <script> alert('Data Berhasil DiSimpan!') </script> ";
			}
			echo " <script> window.location='".site_url('user')."' </script> ";
		}

    }

	public function hapus()
	{
		$id = $this->input->post('user_id');
		$this->M_user->hapus($id);
		
		if($this->db->affected_rows() > 0){
			echo " <script> alert('Data Berhasil di Hapus!') </script> ";
		}
		echo " <script> window.location='".site_url('user')."' </script> ";
	}
}
