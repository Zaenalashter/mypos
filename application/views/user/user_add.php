<section class="content-header">
      <h1>
       Users
        <small> Pengguna</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Users</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add Users</h3>
            <div class="pull-right">
                <a href="<?= site_url('user')?>" class="btn btn-warning btn-flat">
                   <i class="fa fa-undo"></i> Back
                </a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <?php //echo validation_errors(); ?>
                   <form action="" method="post">
                        <div class="form-group <?=form_error('name') ? 'has-error' : null ?> ">
                            <label for="">Name *</label>
                            <input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
                            <?=form_error('name')?>
                        </div>
                        <div class="form-group <?=form_error('username') ? 'has-error' : null ?>">
                            <label for="">Username *</label>
                            <input type="text" name="username" value="<?=set_value('username')?>" class="form-control">
                            <?=form_error('username')?>
                        </div>
                        <div class="form-group <?=form_error('password') ? 'has-error' : null ?>">
                            <label for="">Password *</label>
                            <input type="password" name="password" value="<?=set_value('password')?>" class="form-control">
                            <?=form_error('password')?>
                        </div>
                        <div class="form-group <?=form_error('passconf') ? 'has-error' : null ?>">
                            <label for="">Password Confirmation *</label>
                            <input type="password" name="passconf"  value="<?=set_value('passconf')?>" class="form-control">
                            <?=form_error('passconf')?>
                        </div>
                        <div class="form-group">
                            <label for=""> Address</label>
                            <textarea name="address" class="form-control"><?=set_value('address')?></textarea>
                        </div>
                        <div class="form-group <?=form_error('hak_akses') ? 'has-error' : null ?>">
                            <label for="">Hak Akses *</label>
                            <select  name="hak_akses" class="form-control">
                                <option value="">~Pilih Akses~</option>
                                <option value="admin" <?=set_value('hak_akses') == 'admin' ? "selected" : null ?> >Admin</option></option>
                                <option value="kasir" <?=set_value('hak_akses') == 'kasir' ? "selected" : null ?> >Kasir</option>
                                <?=form_error('hak_akses')?>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat">
                                <i class="fa fa-paper-plane"></i> Save
                            </button>
                            <button type="Reset" class="btn btn-flat">Reset</button>
                        </div>
                   </form>
                </div>
            </div>
        </div> 
    </div>

</section>